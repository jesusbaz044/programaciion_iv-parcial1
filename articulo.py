from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, asc
from sqlalchemy.orm import sessionmaker
#para crear la base de datos

engine = create_engine('sqlite:///articulos.sqlite')

Base = declarative_base()


Session = sessionmaker(bind=engine)

session = Session()

class Tabla(Base):
    __tablename__ = 'articlee'
    id = Column(Integer, primary_key=True)
    articulo = Column(String, default='articulo por defecto')
    categoria = Column(String, default='categoria por defecto')
    marca = Column(String, default='marca por defecto')
    precio = Column(Integer, default=0)

    def __repr__(self):
        return self.articulo
        
Base.metadata.create_all(engine)

class Artic:
    def __init__(self):
        self.filas = session.query(Tabla).all()
        self.opcion =int(input(
        '''     Menú 
        1)Agregar articulo
        2)Editar Articulo
        3)Mostrar Articulos
        4)Buscar Articulo
        5)Eliminar Palabra
        6)Salir\n'''
        ))

    def agregarArticulo(self):
        if self.opcion == 1:
            print("Ingrese el articulo: ")
            art1 = input()
            print("Ingrese la categoria: ")
            cat1 = input()
            print("Ingrese la Marca: ")
            mar = input()
            print("Ingrese el precio: ")
            preci = int(input())
            nuevo_articulo = Tabla(articulo= art1, categoria=cat1, marca= mar, precio = preci)
            session.add(nuevo_articulo)
            session.commit()
            print("Se ha añadido correctamente\n")
    def editarArticulo(self):
        if self.opcion == 2:
            u = session.query(Tabla).order_by(asc(Tabla.id)).all()
            for count, item in enumerate(self.filas):
                print(f'{count + 1}. {item}\n')
            print("Ingrese el numero de el articulo que desea editar: ")
            editar = input()
            for c, i in enumerate(u):
                if int(editar) == c +1:
                    nuevo_art = input("Ingrese el articulo:\n")
                    nuevo_categoria = input("Ingrese la categoria: \n")
                    nueva_marca = input("Ingrese la marca: ")
                    nuevo_precio = int(input("Ingrese el precio: "))
                    new_article= Tabla(articulo= nuevo_art, categoria= nuevo_categoria, marca = nueva_marca, precio = nuevo_precio)
                    session.delete(i)
                    session.commit()
                    session.add(new_article)
                    session.commit()
                        
    def mostrarArticulo(self):
        if self.opcion == 3:
            u = session.query(Tabla).order_by(asc(Tabla.articulo)).all()
            if len(u)> 0:
                for count, item in enumerate(self.filas):
                    print(f'{count + 1}. {item}\n')
            else:
                print("No hay Nada")
    def buscarArticulo(self):
        if self.opcion == 4:
            print("TODAS LOS ARTICULOS\n")
            u = session.query(Tabla).order_by(asc(Tabla.articulo)).all()
            if len(u)> 0:
                for count, item in enumerate(u):
                    print(f'{count + 1}. {item} CATEGORIA:{item.categoria} MARCA:{item.marca} PRECIO:{item.precio}')
            else:
                print("No hay Nada")


    def eliminarArticulo(self):
        if self.opcion ==5:
            u = session.query(Tabla).order_by(asc(Tabla.id)).all()
            for count, item in enumerate(self.filas):
                print(f'{count + 1}. {item}\n')
            print("Ingrese el numero de el articulo que desea eliminar: ")
            borrar = input()
            for c, i in enumerate(u):
                if int(borrar) == c +1:
                    session.delete(i)
                    session.commit()
            print("El articulo ha sido eliminada")
    def cerrar(self):
        if self.opcion == 6:
            print("Gracias por usar nuestro servicio")
            exit()

while True:
    artt1 = Artic()
    artt1.mostrarArticulo()
    artt1.editarArticulo()
    artt1.buscarArticulo()
    artt1.agregarArticulo()
    artt1.eliminarArticulo()
    artt1.cerrar()